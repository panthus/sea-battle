module Lib (run) where

import System.Console.Haskeline (runInputT, outputStrLn, getInputLine, defaultSettings, InputT)
import Control.Monad.State (evalStateT, lift, get, put, StateT)
import Text.Read (readEither)

-- MODEL

{-| The string is the players name. -}
data Player = Player String (Grid Cell)

{-| 
- Create: provide player names
- Place ships: both players place all ships.
- Start: both players place bombs
- Finish: the ships of one of the players are destroyed.
-}
data Game
    = NotCreated
    | ShipsPlayer1 Player Player
    | ShipsPlayer2 Player Player
    | BombPlayer1 Player Player
    | BombPlayer2 Player Player
    | Finished Player Player

data Ship
    = Battleship
    | Cruiser
    | Submarine
    | Destroyer
    deriving (Eq)

data Direction
    = Vertical
    | Horizontal

data Cell
    = Ship Ship
    | Empty
    | Bombed
    | Missed
    deriving (Eq)

newtype Grid a = Grid [[a]]

-- RENDER

data RenderMode = ShowShips | HideShips

class Render a where
    render :: RenderMode -> a -> String

instance Render a => Render [a] where
    render _ [] = "\n"
    render mode (x:xs) = render mode x ++ render mode xs

instance Render a => Render (Grid a) where
    render mode (Grid grid) = render mode grid

instance Render Cell where
    render m@ShowShips (Ship ship) = render m ship
    render HideShips (Ship _) = " . "
    render _ Empty = " . "
    render _ Bombed = " X "
    render _ Missed = " O "

instance Render Ship where
    render _ Battleship = " B "
    render _ Cruiser = " C "
    render _ Submarine = " S "
    render _ Destroyer = " D "

-- LOGIC

emptyGrid :: Int -> Grid Cell
emptyGrid size =
    Grid $ replicate size (replicate size Empty)

shipLength :: Ship -> Int
shipLength Battleship = 5
shipLength Cruiser = 4
shipLength Submarine = 3
shipLength Destroyer = 2

maxShips :: Ship -> Int
maxShips Battleship = 1
maxShips Cruiser = 1
maxShips Submarine = 2
maxShips Destroyer = 1

create :: String -> String -> Game -> Either String Game
create name1 name2 NotCreated = Right $ ShipsPlayer1 (Player name1 $ emptyGrid 10) (Player name2 $ emptyGrid 10)
create name1 name2 _ = Left "The game is already created."

instance Foldable Grid where
    foldr f state (Grid grid) = foldr (flip $ foldr f) state grid

placeShip :: Int -> Int -> Ship -> Direction -> Game -> Either String Game
placeShip x y ship direction game =
    case game of
        ShipsPlayer1 (Player n grid) p ->
            fmap (\g -> ShipsPlayer1 (Player n g) p) $ placeShip'' grid
        ShipsPlayer2 p (Player n grid) ->
            fmap (\g -> ShipsPlayer2 p (Player n g)) $ placeShip'' grid
        _ -> Left "You cannot place ships before the game is created or after the game started."
    where
        length = shipLength ship
        max = maxShips ship
        shipCount grid = foldr (\a b -> if a == Ship ship then b + 1 else b) 0 grid `div` length

        placeShip'' grid | shipCount grid < max = placeShip' x y length grid
        placeShip'' grid = Left $ "Cannot place more than " ++ show max ++ " ships of this type."

        placeShip' _ _ 0 grid' = Right grid'
        placeShip' x' y' length grid' = do
            grid'' <- update place x' y' grid'
            case direction of
                Vertical ->
                    placeShip' x' (y' + 1) (length - 1) grid''
                Horizontal ->
                    placeShip' (x' + 1) y' (length - 1) grid''

        place Empty = Right $ Ship ship
        place _ =  Left "Cannot place a ship here."

switchPlayer :: Game -> Either String Game
switchPlayer game =
    case game of
        ShipsPlayer1 p1 p2 ->
            Right $ ShipsPlayer2 p1 p2
        ShipsPlayer2 p1 p2 ->
            Right $ ShipsPlayer1 p1 p2
        _ -> Left "You cannot manually switch players before the game is created or after the game started."

start :: Game -> Either String Game
start game =
    case game of
        ShipsPlayer1 p1@(Player _ grid1) p2@(Player _ grid2) | allShipsPlaced grid1 grid2 ->
            Right $ BombPlayer2 p1 p2
        ShipsPlayer2 p1@(Player _ grid1) p2@(Player _ grid2) | allShipsPlaced grid1 grid2 ->
            Right $ BombPlayer1 p1 p2
        _ -> Left "You cannot start the game if not all ships have been placed or it is already started."
    where
        requiredShipCount =
            (shipLength Battleship * maxShips Battleship)
            + (shipLength Cruiser * maxShips Cruiser)
            + (shipLength Submarine * maxShips Submarine)
            + (shipLength Destroyer * maxShips Destroyer)

        allShipsPlaced grid1 grid2 =
            totalShipCount grid1 == requiredShipCount && totalShipCount grid2 == requiredShipCount

placeBomb :: Int -> Int -> Game -> Either String Game
placeBomb x y game =
    case game of
        BombPlayer1 p (Player n grid) ->
            fmap (\g -> (if totalShipCount g == 0 then Finished else BombPlayer2) p (Player n g)) $ update bomb x y grid
        BombPlayer2 (Player n grid) p ->
            fmap (\g -> (if totalShipCount g == 0 then Finished else BombPlayer1) (Player n g) p) $ update bomb x y grid
        _ -> Left "You cannot bomb if the game is not created, not all ships are placed or the game is finished."
    where
        bomb (Ship _) = Right Bombed
        bomb Empty = Right Missed
        bomb _ = Left "Already bombed."

totalShipCount :: Grid Cell -> Int
totalShipCount = foldr count 0
    where
        count (Ship _) b = b + 1
        count _ b = b

update :: (Cell -> Either String Cell) -> Int -> Int -> Grid Cell -> Either String (Grid Cell)
update _ x _ _ | x > 10 || x < 1 = Left "X needs to be between 1 and 10"
update _ _ y _ | y > 10 || y < 1 = Left "Y needs to be between 1 and 10"
update updateP x y (Grid grid) =
    let
        rowpref = take (y-1) grid
        (row:rowpost) = drop (y-1) grid
        pref = take (x-1) row
        (pos:post) = drop (x-1) row
        grid' newPos = Grid $ rowpref ++ [pref ++ [newPos] ++ post] ++ rowpost
    in
        grid' <$> updateP pos

handleError :: (Game -> Either String Game) -> (Game -> InputT IO ()) -> StateT Game (InputT IO) ()
handleError action onSuccess = do
    game <- get
    case action game of
        Left error ->
            lift $ outputStrLn error
        Right game' -> do
            put game'
            lift $ onSuccess game'

getActivePlayer :: Game -> Player
getActivePlayer (ShipsPlayer1 p _) = p
getActivePlayer (ShipsPlayer2 _ p) = p
getActivePlayer (BombPlayer1 p _) = p
getActivePlayer (BombPlayer2 _ p) = p

getGrid :: Game -> Grid Cell
getGrid game = let Player _ grid = getActivePlayer game in grid

getOpponentGrid :: Game -> Grid Cell
getOpponentGrid (BombPlayer1 _ (Player _ grid)) = grid
getOpponentGrid (BombPlayer2 (Player _ grid) _) = grid

getName :: Game -> String
getName game = let Player name _ = getActivePlayer game in name

parseShip :: String -> Either String Ship
parseShip "B" = Right Battleship
parseShip "C" = Right Cruiser
parseShip "S" = Right Submarine
parseShip "D" = Right Destroyer
parseShip _ = Left "Cannot parse ship, valid options are B, C, S and D."

parseDirection :: String -> Either String Direction
parseDirection "H" = Right Horizontal
parseDirection "V" = Right Vertical
parseDirection _ = Left "Cannot parse direction, valid options are H and V."

run :: IO ()
run = runInputT defaultSettings (evalStateT loop NotCreated)
    where
        loop :: StateT Game (InputT IO) ()
        loop = do
            input <- lift $ getInputLine "% "
            case maybe [] words input of
                [] -> loop
                ["quit"] -> pure ()
                ["create", name1, name2] -> do
                    handleError (create name1 name2)
                        (\g -> do
                            outputStrLn $ getName g ++ " can now place ships."
                            outputStrLn $ render ShowShips $ getGrid g)
                    loop
                ["switch"] -> do
                    handleError switchPlayer
                        (\g -> do
                            outputStrLn $ getName g ++ " can now place ships."
                            outputStrLn $ render ShowShips $ getGrid g)
                    loop
                ["addShip", x, y, ship, direction] -> do
                    handleError addShip (outputStrLn . render ShowShips . getGrid)
                    loop
                    where
                        addShip grid = do
                            x <- readEither x
                            y <- readEither y
                            ship <- parseShip ship
                            direction <- parseDirection direction
                            placeShip x y ship direction grid
                ["start"] -> do
                    handleError start
                        (\g -> do
                            outputStrLn $ getName g ++ " can now bomb ships."
                            outputStrLn $ render HideShips $ getOpponentGrid g)
                    loop
                ["bomb", x, y] -> do
                    handleError bomb onSuccess
                    loop
                    where
                        bomb grid = do
                            x <- readEither x
                            y <- readEither y
                            placeBomb x y grid

                        onSuccess (Finished (Player n1 g1) (Player n2 _)) =
                            outputStrLn $ (if totalShipCount g1 == 0 then n2 else n1) ++ " won the game!"
                        onSuccess g = do
                            outputStrLn $ render HideShips $ getGrid g
                            outputStrLn $ getName g ++ " can now bomb ships."
                            outputStrLn $ render HideShips $ getOpponentGrid g
                input -> do
                    lift $ outputStrLn $ "Invalid input: " ++ show (unwords input)
                    loop
